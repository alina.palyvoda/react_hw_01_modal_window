import React from "react";
import styles from "./Button.module.scss";

class Button extends React.PureComponent {
  render() {
    const { text, backgroundColor, onClick } = this.props;
    return (
      <button
        onClick={onClick}
        className={styles.mainButton}
        type="button"
        style={{ backgroundColor }}
      >
        {text}
      </button>
    );
  }
}

export default Button;
