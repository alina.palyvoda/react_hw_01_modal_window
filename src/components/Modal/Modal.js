import React from "react";
import styles from "./Modal.module.scss";

class Modal extends React.PureComponent {
  render() {
    const { openModal, closeModal, header, text, closeButton, backgroundColor, actions, id } = this.props;
    
      return (
        <div className={styles.modalContainer}>
          <div onClick={(e) => (e.currentTarget === e.target) && closeModal()} className={styles.background}>
          <div  className={styles.content} style={{ backgroundColor }}>
          <div>
            <h4>{header}</h4>
            {closeButton && <span onClick={() => {closeModal()}}>x</span>}
          </div>
          <p>{text}</p>
          {actions}
        </div>
        </div>
        </div>
      );
    }
  }


export default Modal;
