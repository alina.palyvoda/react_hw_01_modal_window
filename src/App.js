import styles from "./App.module.scss";
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

const modals = [
  {
    id: 1,
    backgroundColor: "gold",
    header: "First Modal",
    text: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.Obcaecati eveniet voluptatem aliquid pariatur facilis explicabo atque soluta quidem dolor",
    actions: (
      <div>
        <button type="submit">YES</button>
        <button type="submit">NO</button>
      </div>
    ),
    closeButton: true,
  },

  {
    id: 2,
    backgroundColor: "deepskyblue",
    header: "Second Modal",
    text: "Ipsa, iure enim perferendis nulla assumenda iusto fugiat veniam deserunt aut cupiditate laborum pariatur mollitia fugit id accusamus quos eveniet voluptatem ut quis dolore facere placeat libero recusandae illum. Aspernatur, nesciunt voluptatum",
    actions: (
      <div>
        <button type="submit">OK</button>
        <button type="submit">CANCEL</button>
      </div>
    ),
    closeButton: true,
  },
];

class App extends React.Component {
  state = {
    openModal: false,
  };

  openModal = (i) => {
    const index = modals.findIndex((el) => i === el.id);
    const modal = modals[index];
    this.setState({ openModal: modal });
  };

  closeModal = () => {
    this.setState({ openModal: false });
  };

  render() {
    const { header, text, backgroundColor, closeButton, actions, id } =
      this.state.openModal;

    return (
      <div className={styles.App}>
        <Button
          onClick={() => {
            this.openModal(1);
          }}
          backgroundColor="gold"
          text="Open first modal"
        />

        <Button
          onClick={() => {
            this.openModal(2);
          }}
          backgroundColor="deepskyblue"
          text="Open second modal"
        />

        {this.state.openModal && (
          <Modal
            openModal={this.openModal}
            closeModal={this.closeModal}
            header={header}
            text={text}
            backgroundColor={backgroundColor}
            closeButton={closeButton}
            actions={actions}
            id={id}
          />
        )}
      </div>
    );
  }
}

export default App;
